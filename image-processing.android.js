var application = require("application");

module.exports = {
    gpuImage: null,
    filter: null,

    invert: function(image) {
        this.gpuImage = new jp.co.cyberagent.android.gpuimage.GPUImage(application.android.context);
        this.gpuImage.setImage(image.android);

        this.gpuImage.setFilter(new jp.co.cyberagent.android.gpuimage.GPUImageColorInvertFilter());

        image.android = this.gpuImage.getBitmapWithFilterApplied();
    },
    addThreshold: function(image, thresholdValue) {
        this.gpuImage = new jp.co.cyberagent.android.gpuimage.GPUImage(application.android.context);
        this.gpuImage.setImage(image.android);

        this.filter = new jp.co.cyberagent.android.gpuimage.GPUImageLuminanceThresholdFilter(thresholdValue);

        this.gpuImage.setFilter(filter);

        image.android = this.gpuImage.getBitmapWithFilterApplied();
    },
    blend: function(image1, image2) {
        this.gpuImage = new jp.co.cyberagent.android.gpuimage.GPUImage(application.android.context);
        this.gpuImage.setImage(image1.android);

        this.filter = new jp.co.cyberagent.android.gpuimage.GPUImageDifferenceBlendFilter();
        this.filter.setBitmap(image2.android);  
        this.gpuImage.setFilter(this.filter);
        image1.android = this.gpuImage.getBitmapWithFilterApplied();

        this.gpuImage = new jp.co.cyberagent.android.gpuimage.GPUImage(application.android.context);
        this.gpuImage.setImage(image1.android);
        this.gpuImage.setFilter(new jp.co.cyberagent.android.gpuimage.GPUImageExposureFilter(0.2));
        image1.android = this.gpuImage.getBitmapWithFilterApplied();

        this.gpuImage = new jp.co.cyberagent.android.gpuimage.GPUImage(application.android.context);
        this.gpuImage.setImage(image1.android);
        this.gpuImage.setFilter(new jp.co.cyberagent.android.gpuimage.GPUImageBrightnessFilter(0.2));

        image1.android = this.gpuImage.getBitmapWithFilterApplied();
    }
}
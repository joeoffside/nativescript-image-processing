//[[GPUImageContext sharedFramebufferCache] purgeAllUnassignedFramebuffers]

module.exports = {
    filter: GPUImageColorInvertFilter.alloc().init(),
    pic1: null,
    pic2: GPUImagePicture.alloc(),
    differenceBlend: GPUImageDifferenceBlendFilter.alloc().init(),
    exposureFilter: GPUImageExposureFilter.alloc().init(),
    brightnessFilter: GPUImageBrightnessFilter.alloc().init(),

    invert: function(image) {
        try {
            image.ios = this.filter.imageByFilteringImage(image.ios);
        } catch (ex) {
            console.log("GPUImage - Error inverting image: " + ex);
        } finally {
            this.filter.removeAllTargets();
            this.filter.removeOutputFramebuffer();
            GPUImageContext.sharedFramebufferCache().purgeAllUnassignedFramebuffers();
        }
    },
    addThreshold: function(image, thresholdValue) {
        var filter = GPUImageLuminanceThresholdFilter.alloc().init();
        filter.threshold = thresholdValue;
        
        var newImage = filter.imageByFilteringImage(image.ios);
        image.ios = newImage;

        GPUImageContext.sharedFramebufferCache().purgeAllUnassignedFramebuffers();
    },
    blend: function(image1, image2) {
        this.exposureFilter.exposure = 0.2;
        this.brightnessFilter.brightness = 0.2;

        try {
            this.pic1 = GPUImagePicture.alloc().initWithImage(image1.ios);
            this.pic2 = GPUImagePicture.alloc().initWithImage(image2.ios);

            this.pic1.addTarget(this.differenceBlend);
            this.pic1.processImage();

            this.pic2.addTarget(this.differenceBlend);
            this.pic2.processImage();

            this.differenceBlend.useNextFrameForImageCapture();
            image1.ios = this.differenceBlend.imageFromCurrentFramebuffer();

            this.brightnessFilter.addTarget(this.exposureFilter);
            image1.ios = this.brightnessFilter.imageByFilteringImage(image1.ios);
        } catch (ex) {
            console.log("GPUImage - Error blending image: " + ex);
        } finally {
            this.pic1.removeAllTargets();
            this.pic1.removeOutputFramebuffer();
            this.pic1 = null;

            this.pic2.removeAllTargets();
            this.pic2.removeOutputFramebuffer();
            this.pic2 = null;

            this.differenceBlend.removeAllTargets();
            this.differenceBlend.removeOutputFramebuffer();

            this.exposureFilter.removeAllTargets();
            this.exposureFilter.removeOutputFramebuffer();

            this.brightnessFilter.removeAllTargets();
            this.brightnessFilter.removeOutputFramebuffer();

            GPUImageContext.sharedFramebufferCache().purgeAllUnassignedFramebuffers();
        }
    }
}
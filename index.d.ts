import { ImageSource } from 'image-source';

/**
 * Invert an image.
 * @param {ImageSource} - The image to invert
 */
export function invert(image: ImageSource);

/**
 * Add threshold to an image
 * @param {ImageSource} - The image to add threshold
 * @param {number} [] 
 */
export function addThreshold(image: ImageSource, thresholdValue: number);

/**
 * Blend two images together
 * @param {ImageSource} - The first image
 * @param {ImageSource} - The second image
 */
export function blend(image1: ImageSource, image2: ImageSource);